import React from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

import { getUsers } from './src/store/actions';

import Header from './src/components/Header/Header';
import LoadingModal from './src/components/LoadingModal/LoadingModal';
import UsersList from './src/components/UsersList/UsersList';
import UserModal from './src/components/UserModal/UserModal';
import Footer from './src/components/Footer/Footer';

class App extends React.Component {

  componentWillMount = () => {
    this.props.getUsers();
  }

  render = () => {
    return (
      <View style={styles.container}>
        <LoadingModal />
        <Header/>
        <View style={styles.listView}>
          <UsersList style={styles.usersList}/>
        </View>
        <View style={styles.footerView}>
          <Footer/>
        </View>
        <UserModal/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listView: {
    flex: 1
  },
  footerView: {
    backgroundColor: "#5594BC"
  }
});

const mapStateToProps = state => ({
  page: state.users.page,
  users: state.users.users,
  selectedUser: state.users.selectedUser
});
const mapDispatchToProps = dispatch => ({
  getUsers: () => {
    dispatch(getUsers());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
