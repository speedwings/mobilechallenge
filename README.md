# MOBILE CHALLENGE

## AVVIO
Seguire la procedura standard per lo scaricamento dei pacchetti Node con
```
npm install
```
e poi avviare l'iPhone Simulator con
```
npm run ios
```
*(non testato su emulatore Android)*

## DESCRIZIONE DEL PROGETTO
L'applicazione è stata sviluppata utilizzando React Native.
Ho approcciato lo sviluppo considerando la lista utenti una sezione di un App destinata ad espandersi per questo ho:
- Implementato la gestione dei dati e della cache con Redux, salvo il filtro e l'ordinamento della lista utenti, limitati al contesto del componente.
- Aggiunto la libreria Moment.js che risulterebbe un footprint eccessivo se utilizzata solo in un punto come adesso.

Di contro non ho integrato un pacchetto per la gestione della navigazione, che risulterebbe probabilmente necessario in un'app con più sezioni. Allo stato attuale avrebbe appesantito il progetto e costretto a "lottare" contro gli stili grafici imposti dalla navigazione nativa per permettermi di personalizzare il lato grafico.

La gestione della cache si articola nelle seguenti fasi:
- Al momento del cambio di pagina vengono richiesti i dati alla cache
- Viene innescata la chiamata asincrona alle API
- In caso di successo vengono aggiornati i dati in cache e nell'elenco attivo
- In caso di fallimento viene impostato un flag

In questo modo l'app risulta più reattiva al cambio di pagina, quando i dati sono presenti in cache, e mostra un comportamento diverso all'utente in caso di problemi nella ricezione o nell'aggiornamento dei dati.

## REFACTORING
- Spostare gli oggetti StyleSheet all'interno di un unico file per uniformare gli stili grafici e ridurre al minimo le dichiarazioni inline
- Rivedere la sequenza di dispatch delle action nel flusso di chiamata asincrona alle API
- Valutare la possibilità di rendere persistente la cache utilizzando il Local Storage
- Reimpostare lo scroll della lista utenti al cambio di pagina

## PROBLEMI NOTI
- **Warning: isMounted(...) is deprecated in plain JavaScript React classes...**: Issue , ancora aperta su React-Native ([#18868](https://github.com/facebook/react-native/issues/18868)).
