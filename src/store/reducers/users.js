import { USER_SELECT, USER_UNSELECT, USERS_REQUEST_SUCCESS, USERS_REQUEST_FAILED, USERS_INCREASE_PAGE, USERS_DECREASE_PAGE, USERS_GET_CACHE } from '../actions/actionTypes';

const initialState = {
    page: 1,
    users: [],
    cache: {},
    failed: false,
    selectedUser: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
      case USER_SELECT:
        return { ...state, selectedUser: state.users.find(user => user.id === action.id) };
      case USER_UNSELECT:
        return { ...state, selectedUser: null };
      case USERS_GET_CACHE:
        return { ...state,  users: state.cache[state.page] || [] };
      case USERS_REQUEST_SUCCESS:
        return {
          ...state,
          cache: Object.assign(state.cache, { [state.page]: action.users } ),
          users: action.users,
          failed: false
        };
      case USERS_REQUEST_FAILED:
        return { ...state, failed: true };
      case USERS_INCREASE_PAGE:
        return { ...state, page: state.page + 1 };
      case USERS_DECREASE_PAGE:
        const page = state.page - 1;
        return { ...state, page: (page > 0) ? page : 1 };
      default:
        return state;
    }
}

export default usersReducer;
