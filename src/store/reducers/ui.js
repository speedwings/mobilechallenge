import { UI_SET_LOADING, UI_UNSET_LOADING } from '../actions/actionTypes';

const initialState = {
    isLoading: false
}

const uiReducer = (state = initialState, action) => {
    switch (action.type) {
      case UI_SET_LOADING:
      case UI_UNSET_LOADING:
        return { ...state, isLoading: (action.type === UI_SET_LOADING) };
      default:
        return state;
    }
}

export default uiReducer;
