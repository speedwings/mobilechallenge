import { UI_SET_LOADING, UI_UNSET_LOADING } from "./actionTypes";

export const uiSetLoading = () => {
    return {
        type: UI_SET_LOADING
    };
}

export const uiUnsetLoading = () => {
    return {
        type: UI_UNSET_LOADING
    };
}
