import { USER_SELECT, USER_UNSELECT, USERS_REQUEST_SUCCESS, USERS_INCREASE_PAGE, USERS_DECREASE_PAGE, USERS_GET_CACHE, USERS_REQUEST_FAILED } from './actionTypes';
import { uiSetLoading, uiUnsetLoading } from './';

export const getUsers = () => {
    return function (dispatch, getState) {
        dispatch(uiSetLoading());
        dispatch(getCache());
        return fetch("https://challenges.tate.cloud/front2018/users?page="+getState().users.page)
            .then(resp => {
                dispatch(uiUnsetLoading());
                return resp.json()
            })
            .then(respJson => {
                const users = respJson.Users.map(user => {
                    const defaultPaymentName = user.PaymentMethods.find(method => method.default).name;
                    return {
                        ...user,
                        defaultPaymentName: defaultPaymentName
                    };
                });
                dispatch(usersRequestSuccess(users));
            })
            .catch(() => {
                dispatch(uiUnsetLoading());
                dispatch(usersRequestFailed());
            })
    }
}

export const getNextUsers = () => {
    return function (dispatch) {
        dispatch(usersIncreasePage());
        return dispatch(getUsers());
    }
}

export const getPreviousUsers = () => {
    return function (dispatch) {
        dispatch(usersDecreasePage());
        return dispatch(getUsers());
    }
}

export const usersIncreasePage = () => {
    return { type: USERS_INCREASE_PAGE };
}

export const usersDecreasePage = () => {
    return { type: USERS_DECREASE_PAGE };
}


export const getCache = () => {
    return { type: USERS_GET_CACHE };
}

export const usersRequestSuccess = users => {
    return { type: USERS_REQUEST_SUCCESS, users: users }
}

export const usersRequestFailed = () => {
    return { type: USERS_REQUEST_FAILED }
}

export const selectUser = id => {
    return { type: USER_SELECT, id: id }
}

export const unselectUser = () => {
    return { type: USER_UNSELECT }
}
