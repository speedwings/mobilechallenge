import React from 'react';
import { View, FlatList, StyleSheet, TextInput, TouchableOpacity, Text, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getUsers } from '../../store/actions';

import UsersListItem from '../UsersListItem/UsersListItem';

const sortableFields = {
    username: 'Nome utente',
    email: 'Indirizzo email',
    defaultPayment: 'Pagamento'
}

class UsersList extends React.Component {
    state = {
        filter: '',
        sortBy: null
    };

    filteredUsers = () => {
        return _.sortBy(this.props.users.filter(user => {
            return user.email.toLowerCase().indexOf(this.state.filter.trim().toLowerCase()) >= 0;
        }), this.state.sortBy);
    }

    showSortAlert = () => {
        const buttons = Object.entries(sortableFields).map(([field, label]) => {
            return {
                text: label,
                onPress: () => this.setState({ sortBy: field }),
                style: (field === this.state.sortBy) ? 'destructive' : 'default'
            }
        });
        buttons.push({
            text: 'Nessuno',
            onPress: () => this.setState({ sortBy: null }),
            style: 'cancel'
        });
        Alert.alert(
            'Ordinamento',
            'Ordina la lista utenti per:',
            buttons
        )
    }

    clearButton = () => {
        return (this.state.filter.trim() === '') ? null : (
            <TouchableOpacity onPress={() => { this.setState({ filter: '' }) }} style={styles.searchButton}>
                <Text style={styles.searchButtonText}>X</Text>
            </TouchableOpacity>
        )
    }

    content = () => {
        if (this.props.users.length === 0) {
            return (
                <View style={styles.noContentView}>
                    <Text style={styles.noContentText}>Errore nella ricezione dei dati</Text>
                    <TouchableOpacity style={styles.noContentButton} onPress={this.props.updateUsers}>
                        <Text style={styles.noContentButtonText}>Aggiorna</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        if (this.filteredUsers().length > 0) {
            return (
                <FlatList
                    data={this.filteredUsers()}
                    renderItem={({ item }) => (<UsersListItem user={item} />)}
                    keyExtractor={user => user.id}
                ></FlatList>
            )
        } else {
            return (
                <View style={styles.noContentView}>
                    <Text style={styles.noContentText}>Nessun risultato trovato per il filtro impostato.</Text>
                    <TouchableOpacity style={styles.noContentButton} onPress={ () => { this.setState({ filter: '' }) } }>
                        <Text style={styles.noContentButtonText}>Mostra tutti</Text>
                    </TouchableOpacity>
                </View>
            )
        }

    }

    render = () => {
        return (
            <View>
                <View style={styles.searchViewContainer}>
                    <View style={styles.searchView}>
                        <TextInput
                            value={this.state.filter}
                            onChangeText={(filter) => this.setState({ filter })}
                            style={styles.searchInput}
                            placeholder="Cerca per email..."
                            autoCapitalize="none"
                            underlineColorAndroid="transparent"/>
                        { this.clearButton() }
                    </View>
                    <TouchableOpacity style={styles.sortButton} onPress={ this.showSortAlert }>
                        <Icon name="ios-funnel" size={24} color="#355B73"/>
                    </TouchableOpacity>
                </View>
                { this.content() }
            </View>
        )
    }
};

const styles = StyleSheet.create({
    searchViewContainer: {
        backgroundColor: "#B5CBDA",
        borderColor: '#65A0C6',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchView: {
        flexDirection: "row",
        padding: 4,
        paddingLeft: 10,
        margin: 6,
        borderWidth: 1,
        borderColor: "#888",
        borderRadius: 20,
        backgroundColor: "white",
        flex: 1
    },
    searchInput: {
        flex: 1,
        fontSize: 16
    },
    searchButton: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#888",
        width: 20,
        height: 20,
        borderRadius: 10
    },
    searchButtonText: {
        color: "white"
    },
    sortButton: {
        padding: 6,
        paddingRight: 10
    },
    noContentView: {
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    noContentText: {
        fontSize: 20,
        fontWeight: '500',
        textAlign: 'center',
        color: '#888'
    },
    noContentButton: {
        backgroundColor: '#FF9A4D',
        borderRadius: 2,
        padding: 12,
        paddingLeft: 18,
        paddingRight: 18,
        marginTop: 20
    },
    noContentButtonText: {
        color: 'white',
        fontWeight: '500'
    }
});

const mapStateToProps = state => ({
    users: state.users.users
});

const mapDispatchToProps = dispatch => ({
    updateUsers: () => {
        dispatch(getUsers());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
