import React from 'react';
import { View, Text, Modal, TouchableHighlight, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { unselectUser } from '../../store/actions';
import { connect } from 'react-redux';
import moment from 'moment';
import IT from 'moment/locale/it';

class UserModal extends React.Component {

    constructor(props) {
        super(props);
        moment.updateLocale("it", IT);
    }

    paymentsListItem = ({item}) => {
        return (
            <View style={{ borderBottomColor: '#355B73', borderBottomWidth: 1, padding: 12 }}>
                <View style={styles.row}>
                    <Text style={[styles.dimmedText, { fontWeight: '500', color: 'white' }]}>{item.name}</Text>
                    <Text style={[styles.dimmedText, styles.smallText]}>{ (item.default) ? ' (Default)' : '' }</Text>
                </View>
                <Text style={[styles.dimmedText, styles.smallText, { fontWeight: '500' }]}>****-****-****-{item.ending_with}</Text>
                <View style={styles.row}>
                    <Text style={[styles.dimmedText, styles.smallText, { fontWeight: '500' }]}>Valuta: </Text>
                    <Text style={[styles.dimmedText, styles.smallText]}>{item.currency}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={[styles.dimmedText, styles.smallText, { fontWeight: '500' }]}>Tipo: </Text>
                    <Text style={[styles.dimmedText, styles.smallText]}>{item.type}</Text>
                </View>
            </View>
        )
    }

    sortedPayments = () => {
        return this.props.user.PaymentMethods.sort(method => method.default).reverse();
    }

    content = () => {
        return (this.props.user !== null) ? (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Icon name="ios-contact" size={56} color="white" style={styles.prefixIcon} />
                    <View>
                        <Text style={styles.whiteText}>{this.props.user.name} {this.props.user.surname}</Text>
                        <Text style={styles.dimmedText}>{this.props.user.username}</Text>
                        <View style={styles.row}>
                            <Icon name="ios-mail" size={20} color="#D8E9F4" style={styles.prefixIcon} />
                            <Text style={styles.dimmedText}>{this.props.user.email}</Text>
                        </View>
                        <View style={styles.row}>
                            <Icon name="ios-calendar" size={20} color="#D8E9F4" style={styles.prefixIcon} />
                            <Text style={styles.dimmedText}>Utente dal {moment(this.props.user.signup_date).format('D MMMM YYYY')}</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles.row, { marginTop: 30 }]}>
                    <Icon name="ios-card" size={30} color="white" style={styles.prefixIcon} />
                    <Text style={styles.whiteText}>Metodi di pagamento</Text>
                </View>
                <View style={styles.list}>
                    <FlatList
                        data={ this.sortedPayments() }
                        renderItem={ this.paymentsListItem }
                        keyExtractor={ payment => payment.id }
                    ></FlatList>
                </View>
            </View>
        ) : null;
    }

    render = () => {
        return (
            <Modal visible={this.props.user !== null} animationType="slide">
                <View style={styles.header}>
                    <TouchableHighlight onPress={this.props.closeHandler} style={styles.closeButton}>
                        <Text style={styles.close}>X</Text>
                    </TouchableHighlight>
                </View>
                { this.content() }
            </Modal>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        padding: 12,
        flex: 1,
        backgroundColor: '#4D697C'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: 12,
        backgroundColor: '#4D697C'
    },
    closeButton: {
        paddingTop: 12,
        paddingRight: 12
    },
    close: {
        fontSize: 20,
        color: 'white'
    },
    row: {
        flexDirection: 'row'
    },
    prefixIcon: {
        marginRight: 12,
    },
    list: {
        flex: 1
    },
    dimmedText: {
        color: '#D8E9F4',
        fontSize: 16
    },
    whiteText: {
        color: 'white',
        fontSize: 24,
        fontWeight: '600'
    },
    smallText: {
        fontSize: 12
    }
});

const mapStateToProps = state => ({
    user: state.users.selectedUser
});

const mapDispatchToProps = dispatch => ({
    closeHandler: () => { dispatch(unselectUser()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserModal);
