import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { selectUser } from '../../store/actions';

const UsersListItem = props => (
    <View style={styles.row}>
        <View style={styles.details}>
            <Text style={styles.nameText}>{props.user.username}</Text>
            <View style={{ flexDirection: 'row' }}>
                <Icon name="ios-mail" size={18} color="#888" style={{ marginRight: 4 }}/>
                <Text>{props.user.email}</Text>
            </View>
            <Text style={styles.paymentText}>{ props.user.defaultPaymentName }</Text>
        </View>
        <TouchableOpacity key={props.user.id} onPress={() => props.pressHandler(props.user.id)} style={styles.button}>
            <Text style={styles.buttonText}>Dettagli</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    borderBottomColor: "#eee",
    borderBottomWidth: 1,
    padding: 10,
    alignItems: 'center'
  },
  details: {
    flex: 1
  },
  nameText: {
      fontSize: 14
  },
  paymentText: {
      color: '#666'
  },
  button: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 14,
    paddingRight: 14,
    backgroundColor: "#FF9A4D",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 2
  },
  buttonText: {
    color: "white",
    fontWeight: '600'
  }
});

const mapDispatchToProps = dispatch => ({
    pressHandler: id => {
        dispatch(selectUser(id))
    }
});

export default connect(null, mapDispatchToProps)(UsersListItem);
