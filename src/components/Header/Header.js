import React from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { getUsers } from '../../store/actions';

class Header extends React.Component {

    errorToggle = () => {
        return (this.props.error) ? (
            <TouchableOpacity onPress={ this.showAlert }>
                <Icon name="ios-alert" size={30} style={styles.errorToggle} color="white"/>
            </TouchableOpacity>
        ) : null;
    }

    showAlert = () => {
        Alert.alert(
            'Dati non aggiornati',
            'Errore nella ricezione dei dati. Vuoi provare ad aggiornare?',
            [
                { text: 'Chiudi', style: 'cancel' },
                { text: 'Aggiorna', onPress: this.props.update },
            ]
        )
    }

    render = () => {
        return (
            <View style={styles.view}>
                <Icon name="ios-people" size={40} color="white" style={styles.icon} />
                <Text style={styles.title}>Utenti</Text>
                { this.errorToggle() }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 30,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: "#5594BC"
    },
    icon: {
        marginRight: 10
    },
    title: {
        fontSize: 30,
        fontWeight: "600",
        color: "white",
        flex: 1
    },
    errorToggle: {
        marginTop: 10,
        marginRight: 10
    }
})

const mapStateToProps = state => ({
    error: state.users.failed && state.users.users.length > 0
});

const mapDispatchToProps = dispatch => ({
    update: () => {
        dispatch(getUsers())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
