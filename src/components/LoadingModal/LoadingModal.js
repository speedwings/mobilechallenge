import React from 'react';
import { View, Text, Modal, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

const LoadingModal = props => (
    <Modal visible={props.isLoading} transparent={true}>
        <View style={styles.modalView}>
            <ActivityIndicator color="white"/>
            <Text style={styles.text}>Caricamento in corso...</Text>
        </View>
    </Modal>
);

const styles = StyleSheet.create({
    modalView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.8)',
    },
    text: {
        color: 'white'
    }
});

const mapStateToProps = state => ({
    isLoading: state.ui.isLoading
});

export default connect(mapStateToProps)(LoadingModal);
