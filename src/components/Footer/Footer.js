import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { getNextUsers, getPreviousUsers } from '../../store/actions';

const Footer = props => (
  <View style={styles.container}>
    <TouchableOpacity style={[styles.arrow, props.page === 1 && styles.disabled]} onPress={props.previousPage} disabled={props.page === 1}>
      <Icon name="ios-arrow-back" size={22} color="white" />
    </TouchableOpacity>
    <Text style={styles.page}>Pag. {props.page}</Text>
    <TouchableOpacity style={styles.arrow} onPress={props.nextPage}>
      <Icon name="ios-arrow-forward" size={22} color="white" />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  arrow: {
    padding: 8,
    paddingLeft: 12,
    paddingRight: 12
  },
  disabled: {
    opacity: 0.2
  },
  page: {
    padding: 8,
    fontSize: 20,
    color: "white"
  }
});

const mapStateToProps = state => ({
    page: state.users.page
});

const mapDispatchToProps = dispatch => ({
    nextPage: () => {
        dispatch(getNextUsers())
    },
    previousPage: () => {
        dispatch(getPreviousUsers())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
