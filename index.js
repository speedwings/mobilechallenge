import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './src/store/configure';
import App from './App';

const store = configureStore();

const ReduxApp = () => (
    <Provider store={store}>
        <App></App>
    </Provider>
);

AppRegistry.registerComponent("mobilechallenge", () => ReduxApp);
